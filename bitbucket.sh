#!/bin/bash


clear

git clone git@bitbucket.org:RafaelBroseghini/swdt-class-todo.git

cd swdt-class-todo/
find . -type f -name  "*.pyc" -exec rm {} +
find . -type d -name "__pycache__" -exec rm -rf  {} +
find . -type f -name "*.class" -exec rm {} +
find . -type f -name "*.out" -exec rm {} +
find . -type f -name "*.txt" -exec rm {} +


git add .
git commit -m 'Cleaned repository'
cd ~
cp ~/bitbucket.sh  swdt-class-todo/

cd swdt-class-todo/

git push
